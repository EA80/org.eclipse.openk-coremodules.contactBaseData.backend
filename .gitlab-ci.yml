variables:
    FF_USE_FASTZIP: "true" # enable fastzip - a faster zip implementation that also supports level configuration.
    ARTIFACT_COMPRESSION_LEVEL: default 
    CACHE_COMPRESSION_LEVEL: default 
    TRANSFER_METER_FREQUENCY: 5s # will display transfer progress every 5 seconds for artifacts and remote caches.   
    MAVEN_OPTS: "-Djava.awt.headless=true -Dmaven.repo.local=.m2/repository"
    MAVEN_CLI_OPTS: "--batch-mode --errors --fail-at-end --show-version"
    SONAR_USER_HOME: ${CI_PROJECT_DIR}/.sonar  # Defines the location of the analysis task cache
    GIT_DEPTH: 0  # Tells git to fetch all the branches of the project, required by the analysis task
    FEATURE_BRANCH:
      value: "" # this would be the default value
      description: "This variable makes cakes delicious" # makes this variable appear on the Run Pipeline
    PACKAGE_REGISTRY_URL: "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/"

cache:
  key: ${CI_COMMIT_REF_SLUG}
  paths:
    - .m2/repository/

image: maven:3.6-jdk-11
#-----------------------------------------------------------------------------------------------------------------------
stages:  
  - Build
  - Test
  - Sonarqube
  - Dockerimage
  - Upload
  - Deploy-Flyway
  - Deploy
  - Release

#-----------------------------------------------------------------------------------------------------------------------
# Build
#-----------------------------------------------------------------------------------------------------------------------
stage-build:
  image: maven:3.6-jdk-11
  stage: Build
  cache:
    key: ${CI_COMMIT_REF_SLUG}
    paths:
      - .m2/repository/
      - target/
  script:
    - mvn clean package -DskipTests=true
  artifacts:
    paths:
      - target/*.jar


#-----------------------------------------------------------------------------------------------------------------------
# Test
#-----------------------------------------------------------------------------------------------------------------------
stage-test:
  stage: Test
  image: maven:3.6-jdk-11
  script:
    - mvn test -Dskip.asciidoc=true
  artifacts:
     paths:
       - target/     
       - src/     
     reports:
      junit:
       - target/surefire-reports/TEST-*.xml


#-----------------------------------------------------------------------------------------------------------------------
# Sonarqube
#-----------------------------------------------------------------------------------------------------------------------
sonarqube-check:
  stage: Sonarqube
  image:
    name: sonarsource/sonar-scanner-cli:4.6
    entrypoint: [""]
  cache:
    key: ${CI_JOB_NAME}
    paths:
      - .sonar/cache
  script:
    - sonar-scanner -Dsonar.qualitygate.wait=true
  allow_failure: true
  dependencies:
    - stage-test


#-----------------------------------------------------------------------------------------------------------------------
# Dockerimage
#-----------------------------------------------------------------------------------------------------------------------
.docker-build-script:
  image: docker:20.10.7-git
  services:
  - docker:dind
  before_script:
  - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY
  script:
    - ls -l
    - |
      if [[ "$CI_COMMIT_TAG" == "" ]]; then
        tag="sha-${CI_COMMIT_SHORT_SHA}"
      else
        tag="$CI_COMMIT_TAG"
      fi
    - echo current tag ${tag}
    - CI_SOURCE_BRANCH=$(git for-each-ref | grep $CI_COMMIT_SHA | grep origin | sed "s/.*\///" | tr '[:upper:]' '[:lower:]')
    - echo CI_SOURCE_BRANCH $CI_SOURCE_BRANCH
    - REGISTRY_IMAGE_BASE="$CI_REGISTRY_IMAGE/$CI_SOURCE_BRANCH/$IMG_NAME"
    - FINAL_REGISTRY_IMAGE="$CI_REGISTRY_IMAGE/$CI_SOURCE_BRANCH/$IMG_NAME:${tag}"
    - echo "FINAL_REGISTRY_IMAGE=$FINAL_REGISTRY_IMAGE" >> dockerimage.env
    - echo "REGISTRY_IMAGE_BASE=$REGISTRY_IMAGE_BASE" >> dockerimage.env
    - echo "IMAGE_TAG=$tag" >> dockerimage.env
    - echo current FINAL_REGISTRY_IMAGE ${FINAL_REGISTRY_IMAGE}
    - docker build --pull -f $DOCKER_FILE -t "$FINAL_REGISTRY_IMAGE" -t "$REGISTRY_IMAGE_BASE" --cache-from "$REGISTRY_IMAGE_BASE"  --build-arg BUILDKIT_INLINE_CACHE=1 .
    - docker push "$REGISTRY_IMAGE_BASE" --all-tags
  artifacts:
    reports:
      dotenv: dockerimage.env


#-----------------------------------------------------------------------------------------------------------------------
docker-build-flyway:
#-----------------------------------------------------------------------------------------------------------------------
  stage: Dockerimage
  extends: .docker-build-script
  variables:
    IMG_NAME: "cdb-flyway"
    DOCKER_FILE: "Dockerfile_Flyway"
  needs: []    
  #only:
  #  changes:
  #    - Dockerfile_Flyway
  #    - src/main/resources/db/migration/*


#-----------------------------------------------------------------------------------------------------------------------
docker-build-main:
#-----------------------------------------------------------------------------------------------------------------------
  stage: Dockerimage
  extends: .docker-build-script
  variables:
    IMG_NAME: "cdb-main"
    DOCKER_FILE: "Dockerfile"
  needs:
    - job: stage-build


#-----------------------------------------------------------------------------------------------------------------------
# Upload Artefakte BE
#-----------------------------------------------------------------------------------------------------------------------
upload_artefacts:
  stage: Upload
  image: alpine:3.14.0
  rules:
    - if: $CI_COMMIT_TAG
  before_script:
    - apk add --no-cache git curl bash coreutils zip
  script:
    - echo PACKAGE_REGISTRY_URL $PACKAGE_REGISTRY_URL
    - echo CI_COMMIT_TAG $CI_COMMIT_TAG
    - cp ./target/contact-base-data.jar ./contact-base-data.jar
    - |
      curl --header "JOB-TOKEN: ${CI_JOB_TOKEN}" --upload-file ./contact-base-data.jar "${PACKAGE_REGISTRY_URL}${CI_COMMIT_TAG}/artefakte/contact-base-data.jar"


#-----------------------------------------------------------------------------------------------------------------------
# Deploy
#-----------------------------------------------------------------------------------------------------------------------
.deploy-script:
  image: alpine:3.14.0
  cache: {}
  variables:
    GIT_STRATEGY: none
    DEPLOYMENT_FILE: fileDefaultVarPlaceholder 
    YAML_IMAGE_NAME: image
  before_script:
    - apk add --no-cache git curl bash coreutils
    - apk add yq --repository=http://dl-cdn.alpinelinux.org/alpine/edge/community
    - ls -l    
    - git clone https://${CI_USERNAME}:${CI_PUSH_TOKEN}@gitlab.com/${GITLAB_DEPLOYMENT_REPO_URL}
    - cd *
    - git config --global user.email "gitlab@gitlab.com"
    - git config --global user.name "GitLab CI/CD"
  script:
    - ls -l    
    - cat ${DEPLOYMENT_FILE}
    - echo FINAL_REGISTRY_IMAGE ${FINAL_REGISTRY_IMAGE}
    - echo APP_NAME ${APP_NAME}
    - echo IMAGE_TAG ${IMAGE_TAG}
    - echo REGISTRY_IMAGE_BASE ${REGISTRY_IMAGE_BASE}
    - yq e -i '.apps.[env(YAML_APP_NAME)][env(YAML_IMAGE_NAME)].tag = env(IMAGE_TAG)' ${DEPLOYMENT_FILE}
    - yq e -i '.apps.[env(YAML_APP_NAME)][env(YAML_IMAGE_NAME)].repository = env(REGISTRY_IMAGE_BASE)' ${DEPLOYMENT_FILE}
    - cat ${DEPLOYMENT_FILE}
    - git commit -am '[skip ci] Image update'
    - git push origin main


#------------------------------
# Deploy - QA-Environment
#------------------------------
deploy-qa-flyway:
  stage: Deploy-Flyway
  extends: .deploy-script
  variables:
    YAML_APP_NAME: contactbasedata-be
    YAML_IMAGE_NAME: imageDatabase      
    DEPLOYMENT_FILE: deployment/applications/values-cbd-qa.yaml
  dependencies:
    - "docker-build-flyway"
  rules:
    - if: $CI_COMMIT_BRANCH == "master"
      #changes:
      #  - Dockerfile_Flyway
      #  - src/main/resources/db/migration/*

deploy-qa-main:
  stage: Deploy
  extends: .deploy-script
  variables:
    YAML_APP_NAME: contactbasedata-be    
    DEPLOYMENT_FILE: deployment/applications/values-cbd-qa.yaml
  dependencies:
    - "docker-build-main"      
  only:
    - master


#------------------------------
# Deploy - DEV-Environment
#------------------------------
deploy-dev-flyway:
  stage: Deploy-Flyway
  extends: .deploy-script
  variables:
    YAML_APP_NAME: contactbasedata-be
    YAML_IMAGE_NAME: imageDatabase      
    DEPLOYMENT_FILE: deployment/applications/values-cbd-dev.yaml
  dependencies:
    - "docker-build-flyway"
  rules:
    - if: $CI_COMMIT_BRANCH == "DEVELOP"
      changes:
        - Dockerfile_Flyway
        - src/main/resources/db/migration/* 

deploy-dev-main:
  stage: Deploy
  extends: .deploy-script
  variables:
    YAML_APP_NAME: contactbasedata-be    
    DEPLOYMENT_FILE: deployment/applications/values-cbd-dev.yaml
  dependencies:
    - "docker-build-main"      
  only:
    - DEVELOP    


#------------------------------
# Release
#------------------------------
release_job:
  stage: Release
  image: registry.gitlab.com/gitlab-org/release-cli:latest
  needs:
    - job: upload_artefacts
      artifacts: false
    - job: docker-build-main
      artifacts: true
  only:
    - tags
  script:
    - echo 'running release_job'
    - echo FINAL_REGISTRY_IMAGE ${FINAL_REGISTRY_IMAGE}
  release:
    name: 'Release $CI_COMMIT_TAG'
    description: 'Image can be pulled via docker: docker pull ${FINAL_REGISTRY_IMAGE}'
    tag_name: '$CI_COMMIT_TAG'
    ref: '$CI_COMMIT_SHA'
    assets:
      links:
        - name: 'contact-base-data.jar (Download)'
          url:   "${PACKAGE_REGISTRY_URL}${CI_COMMIT_TAG}/artefakte/contact-base-data.jar"