/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.contactbasedata.controller;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.log4j.Log4j2;
import org.eclipse.openk.contactbasedata.exceptions.BadRequestException;
import org.eclipse.openk.contactbasedata.service.CommunicationService;
import org.eclipse.openk.contactbasedata.viewmodel.CommunicationDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;
import java.util.UUID;

@Log4j2
@RestController
@RequestMapping("contacts/")
public class CommunicationController {
    @Autowired
    private CommunicationService communicationService;

    @GetMapping("/{contactUuid}/communications")
    @Secured({"ROLE_KON-READER", "ROLE_KON-WRITER", "ROLE_KON-ADMIN"})
    @ApiOperation(value = "Anzeigen aller Kommunikationswege zu einem Kontakt")
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Ungültige Anfrage."),
            @ApiResponse(code = 200, message = "Suche durchgeführt")})
    @ResponseStatus(HttpStatus.OK)
    public List<CommunicationDto> getContactCommunications(@PathVariable UUID contactUuid) {
        return communicationService.getCommunicationsByContactUuid(contactUuid);
    }

    @GetMapping("/{contactUuid}/communications/{communicationUuid}")
    @Secured({"ROLE_KON-READER", "ROLE_KON-WRITER", "ROLE_KON-ADMIN"})
    @ApiOperation(value = "Einen bestimmten Kommunikationsweg eines bestimmten Kontakts anzeigen.")
    @ApiResponses(value = {
            @ApiResponse(code = 404, message = "Kontaktadresse nicht gefunden."),
            @ApiResponse(code = 400, message = "Ungültige Anfrage."),
            @ApiResponse(code = 200, message = "Kontaktadresse erfolgreich gelesen.")})
    public CommunicationDto readCommunication(
            @PathVariable UUID contactUuid,
            @PathVariable UUID communicationUuid) {

        return communicationService.getCommunication(contactUuid, communicationUuid);
    }


    @PostMapping("/{contactUuid}/communications")
    @Secured({"ROLE_KON-WRITER", "ROLE_KON-ADMIN"})
    @ApiOperation(value = "Anlegen eines neuen Kommunikationswegs")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Kommunikationsweg erfolgreich angelegt"),
            @ApiResponse(code = 500, message = "Konnte nicht durchgeführt werden")
    })
    public ResponseEntity<CommunicationDto> insertCommunication(
            @PathVariable UUID contactUuid,
            @Validated @RequestBody CommunicationDto communicationDto) {
        if (!contactUuid.equals(communicationDto.getContactUuid())) {
            throw new BadRequestException("invalid.uuid.path.object");
        }
        CommunicationDto savedCommunicationDto = communicationService.insertCommunication(contactUuid, communicationDto);
        URI location = ServletUriComponentsBuilder
                .fromCurrentRequestUri()
                .path("/{uuid}")
                .buildAndExpand(savedCommunicationDto.getUuid())
                .toUri();
        return ResponseEntity.created(location).body(savedCommunicationDto);
    }


    @PutMapping("/{contactUuid}/communications/{communicationUuid}")
    @Secured({"ROLE_KON-WRITER", "ROLE_KON-ADMIN"})
    @ApiOperation(value = "Einen bestimmten Kommunikationsweg eines bestimmten Kontakts bearbeiten.")
    @ApiResponses(value = {
            @ApiResponse(code = 404, message = "Kommunikationsweg nicht gefunden."),
            @ApiResponse(code = 400, message = "Ungültige Anfrage."),
            @ApiResponse(code = 200, message = "Kommunikationsweg erfolgreich geändert.")})
    public ResponseEntity updateCommunication(
            @PathVariable UUID contactUuid,
            @PathVariable UUID communicationUuid,
            @Validated @RequestBody CommunicationDto communicationDto) {
        if (!communicationUuid.equals(communicationDto.getUuid())) {
            throw new BadRequestException("invalid.uuid.path.object");
        }
        communicationService.updateCommunication(contactUuid, communicationDto);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("{contactUuid}/communications/{communicationUuid}")
    @Secured({"ROLE_KON-WRITER", "ROLE_KON-ADMIN"})
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(value = "Einen bestimmten Kommunikationsweg eines bestimmten Kontakts löschen")
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "Erfolgreich gelöscht"),
            @ApiResponse(code = 400, message = "Ungültige Anfrage"),
            @ApiResponse(code = 404, message = "Nicht gefunden")})
    public void deleteCommunication(@PathVariable("contactUuid") UUID contactUuid,
                                    @PathVariable("communicationUuid") UUID communicationUuid) {
        communicationService.deleteCommunication(contactUuid, communicationUuid);
    }

}
