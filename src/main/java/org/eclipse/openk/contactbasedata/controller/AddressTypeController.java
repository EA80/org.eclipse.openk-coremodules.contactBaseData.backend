/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.contactbasedata.controller;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.log4j.Log4j2;
import org.eclipse.openk.contactbasedata.exceptions.BadRequestException;
import org.eclipse.openk.contactbasedata.exceptions.ConflictException;
import org.eclipse.openk.contactbasedata.service.AddressTypeService;
import org.eclipse.openk.contactbasedata.viewmodel.AddressTypeDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;
import java.util.UUID;

@Log4j2
@RestController
@RequestMapping("/addressTypes")
public class AddressTypeController {
    @Autowired
    private AddressTypeService addressTypeService;

    @ApiOperation(value = "Anzeigen aller Adresstypen")
    @Secured({"ROLE_KON-READER", "ROLE_KON-WRITER", "ROLE_KON-ADMIN"})
    @ApiResponses(value = { @ApiResponse(code = 200, message = "Erfolgreich durchgeführt")})
    @ResponseStatus(HttpStatus.OK)
    @GetMapping
    public List<AddressTypeDto> getAddressType() {
        return addressTypeService.findAllAddressTypes();
    }



    @GetMapping("/{addressTypeUuid}")
    @Secured({"ROLE_KON-READER", "ROLE_KON-WRITER", "ROLE_KON-ADMIN"})
    @ApiOperation(value = "Suchen eines Adresstyps per UUID")
    @ResponseStatus(HttpStatus.OK)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Ungültige Anfrage."),
            @ApiResponse(code = 200, message = "Adresstyp gefunden")})
    public AddressTypeDto getAddressType(@PathVariable UUID addressTypeUuid) {

        return addressTypeService.getAddressTypeByUuid(addressTypeUuid);
    }


    @PostMapping
    @Secured("ROLE_KON-ADMIN")
    @ApiOperation(value = "Anlegen eines neuen Adresstyps")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Adresstyp erfolgreich angelegt"),
            @ApiResponse(code = 500, message = "Konnte nicht durchgeführt werden")
    })
    public ResponseEntity<AddressTypeDto> insertAddressType(@Validated @RequestBody AddressTypeDto addressTypeDto) {
        AddressTypeDto savedAddressTypeDto = addressTypeService.insertAddressType(addressTypeDto);
        URI location = ServletUriComponentsBuilder
                .fromCurrentRequestUri()
                .path("/{uuid}")
                .buildAndExpand(savedAddressTypeDto.getUuid())
                .toUri();
        return ResponseEntity.created(location).body(savedAddressTypeDto);
    }


    @PutMapping("/{addressTypeUuid}")
    @Secured("ROLE_KON-ADMIN")
    @ApiOperation(value = "Ändern eines Adresstyp")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Adresstyp wurde erfolgreich aktualisiert"),
            @ApiResponse(code = 400, message = "Ungültige Eingabe"),
            @ApiResponse(code = 404, message = "Nicht gefunden")})
    public ResponseEntity updateAddressType(@PathVariable UUID addressTypeUuid, @Validated @RequestBody AddressTypeDto addressTypeDto) {

        if (!addressTypeDto.getUuid().equals(addressTypeUuid)) {
            throw new BadRequestException("invalid.uuid.path.object");
        }

        addressTypeService.updateAddressType(addressTypeDto);
        return ResponseEntity.ok().build();
    }


    @DeleteMapping("/{uuid}")
    @Secured("ROLE_KON-ADMIN")
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(value = "Adresstyp löschen")
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "Erfolgreich durchgeführt"),
            @ApiResponse(code = 400, message = "Ungültige Anfrage"),
            @ApiResponse(code = 404, message = "Nicht gefunden"),
            @ApiResponse(code = 409, message = "Datensatz konnte nicht gelöscht werden")})
    public void removeAddressType(@PathVariable UUID uuid) {
        try {
            addressTypeService.removeAddressType(uuid);
        }
        catch( Exception ex ) {
            throw new ConflictException("Address type couldn't be deleted");
        }
    }

}
