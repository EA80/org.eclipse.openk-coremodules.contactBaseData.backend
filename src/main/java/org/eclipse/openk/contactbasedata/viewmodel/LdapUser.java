package org.eclipse.openk.contactbasedata.viewmodel;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.naming.Name;
import java.nio.charset.StandardCharsets;

//@Entry(base = "ou=users", objectClasses = { "person", "inetOrgPerson", "top" })
public class LdapUser {

    @JsonIgnore
    private Name dn;

    private String uid;
    private String fullName;
    private String firstName;
    private String lastName;
    private String title;
    private String department;
    private String mail;
    private String telephoneNumber;

    private byte[] password;

    public LdapUser() {
    }

    public LdapUser(String uid, String password) {
    }

    public Name getDn() {
        return dn;
    }

    public void setDn(Name dn) {
        this.dn = dn;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public byte[] getPassword() {
        return password;
    }

    public void setPassword(byte[] password) {
        this.password = password;
    }

    public void setPassword2(String password) {
        this.password = password.getBytes(StandardCharsets.UTF_8);
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTelephoneNumber() {
        return telephoneNumber;
    }

    public void setTelephoneNumber(String telephoneNumber) {
        this.telephoneNumber = telephoneNumber;
    }

    @Override
    public String toString() {
        return uid;
    }

}
