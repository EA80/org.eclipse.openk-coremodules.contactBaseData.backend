/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.contactbasedata.service;

import lombok.extern.log4j.Log4j2;
import org.eclipse.openk.contactbasedata.constants.Constants;
import org.eclipse.openk.contactbasedata.enums.OperationType;
import org.eclipse.openk.contactbasedata.exceptions.BadRequestException;
import org.eclipse.openk.contactbasedata.exceptions.NotFoundException;
import org.eclipse.openk.contactbasedata.exceptions.OperationDeniedException;
import org.eclipse.openk.contactbasedata.mapper.CommunicationMapper;
import org.eclipse.openk.contactbasedata.model.RefCommunicationType;
import org.eclipse.openk.contactbasedata.model.TblCommunication;
import org.eclipse.openk.contactbasedata.model.TblContact;
import org.eclipse.openk.contactbasedata.repository.CommunicationRepository;
import org.eclipse.openk.contactbasedata.repository.CommunicationTypeRepository;
import org.eclipse.openk.contactbasedata.repository.ContactRepository;
import org.eclipse.openk.contactbasedata.viewmodel.CommunicationDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Log4j2
@Service
public class CommunicationService {
    @Autowired
    private CommunicationRepository communicationRepository;

    @Autowired
    private CommunicationTypeRepository communicationTypeRepository;

    @Autowired
    private ContactRepository contactRepository;

    @Autowired
    CommunicationMapper communicationMapper;


    public List<CommunicationDto> getCommunicationsByContactUuid(UUID contactUuid) {
        List<TblCommunication> tblCommunicationList = communicationRepository
                .findByTblContactUuid(contactUuid);

        return tblCommunicationList.stream().map(communicationMapper::toCommunicationDto).collect(Collectors.toList());
    }


    public CommunicationDto getCommunication(UUID contactUuid, UUID communicationUuid){
        TblCommunication tblCommunication = communicationRepository.findByUuid(communicationUuid)
                .orElseThrow( () -> new NotFoundException(Constants.COMMUNICATION_UUID_NOT_EXISTING));
        if(!tblCommunication.getTblContact().getUuid().equals(contactUuid)) {
            throw new BadRequestException("invalid.uuid.path.object");
        }
        return communicationMapper.toCommunicationDto(tblCommunication);
    }

    @Transactional
    public CommunicationDto insertCommunication(UUID contactUuid, CommunicationDto communicationDto) {

        TblContact tblContact = contactRepository
                                .findByUuid(contactUuid)
                                .orElseThrow(() -> new NotFoundException(Constants.CONTACT_UUID_NOT_EXISTING));

        RefCommunicationType refCommunicationType = communicationTypeRepository
                .findByUuid(communicationDto.getCommunicationTypeUuid())
                .orElseThrow(() -> new NotFoundException(Constants.COMMUNICATION_UUID_NOT_EXISTING));


        TblCommunication communicationToSave = communicationMapper.toTblCommunication(communicationDto);
        communicationToSave.setUuid(UUID.randomUUID());
        communicationToSave.setTblContact(tblContact);
        communicationToSave.setRefCommunicationType(refCommunicationType);

        if ( !checkUniqueCommunicationTypeForContactForInsert(tblContact.getId(), refCommunicationType.getId())) {
            throw new OperationDeniedException(OperationType.INSERT, Constants.COMMUNICATION_TYPE_ALREADY_EXISTING_FOR_CONTACT);
        }

        setFromCommunicationDto(communicationToSave, communicationDto);

        TblCommunication savedCommunication = communicationRepository.save(communicationToSave);
        return communicationMapper.toCommunicationDto(savedCommunication);
    }

    @Transactional
    public CommunicationDto updateCommunication(UUID contactUuid, CommunicationDto communicationDto) {

        TblContact contact = contactRepository.findByUuid(contactUuid)
                .orElseThrow(() -> new NotFoundException(Constants.CONTACT_UUID_NOT_EXISTING));

        TblCommunication communication = communicationRepository.findByUuid(communicationDto.getUuid())
                .orElseThrow(() -> new NotFoundException(Constants.COMMUNICATION_UUID_NOT_EXISTING));

        RefCommunicationType refCommunicationType = communicationTypeRepository
                .findByUuid(communicationDto.getCommunicationTypeUuid())
                .orElseThrow(() -> new NotFoundException(Constants.COMMUNICATION_TYPE_UUID_NOT_EXISTING));

        TblCommunication communicationToSave = communicationMapper.toTblCommunication(communicationDto);
        communicationToSave.setTblContact(contact);
        communicationToSave.setRefCommunicationType(refCommunicationType);
        communicationToSave.setId(communication.getId());

        if ( !checkUniqueCommunicationTypeForContactForUpdate(contact.getId(), refCommunicationType.getId(), communication.getUuid())) {
            throw new OperationDeniedException(OperationType.UPDATE, Constants.COMMUNICATION_TYPE_ALREADY_EXISTING_FOR_CONTACT);
        }

        setFromCommunicationDto(communicationToSave, communicationDto);
        TblCommunication savedCommunication = communicationRepository.save(communicationToSave);
        return communicationMapper.toCommunicationDto(savedCommunication);
    }


    @Transactional
    public void deleteCommunication(UUID contactUuid, UUID communicationUuid) {
        TblContact tblContact = contactRepository.findByUuid(contactUuid)
                .orElseThrow(() -> new NotFoundException(Constants.CONTACT_UUID_NOT_EXISTING));
        TblCommunication tblCommunication = communicationRepository.findByTblContactAndUuid(tblContact, communicationUuid)
                .orElseThrow(() -> new NotFoundException(Constants.COMMUNICATION_UUID_NOT_EXISTING));

        communicationRepository.delete(tblCommunication);
    }

    private boolean checkUniqueCommunicationTypeForContactForInsert(Long contactId, Long communicationTypeId){
        return communicationRepository.countByContactIdAndCommunicationTypeId(contactId, communicationTypeId) == 0;
    }

    private boolean checkUniqueCommunicationTypeForContactForUpdate( Long contactId, Long communicationTypeId, UUID communicationUuid){
       Long result = communicationRepository.countByContactIdAndCommunicationTypeIdAndIsNotSame(contactId, communicationTypeId, communicationUuid) ;
       return result==0;
    }

    private void setFromCommunicationDto( TblCommunication destTblCommunication, CommunicationDto sourceDto ) {

        if( sourceDto.getCommunicationTypeUuid() != null ) {
            destTblCommunication.setRefCommunicationType( communicationTypeRepository
                    .findByUuid(sourceDto.getCommunicationTypeUuid())
                    .orElseThrow(() -> new NotFoundException(Constants.COMMUNICATION_TYPE_UUID_NOT_EXISTING)));
        }
        else {
            destTblCommunication.setRefCommunicationType(null);
        }
    }

}
