/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.contactbasedata.service;

import lombok.extern.log4j.Log4j2;
import org.eclipse.openk.contactbasedata.constants.Constants;
import org.eclipse.openk.contactbasedata.exceptions.InternalServerErrorException;
import org.eclipse.openk.contactbasedata.model.TblContact;
import org.eclipse.openk.contactbasedata.viewmodel.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.Date;
import java.util.UUID;

@Log4j2
@Service
public class ContactAnonymizerService {
    @Autowired
    ContactService contactService;

    @Autowired
    private CompanyService companyService;

    @Autowired
    private ExternalPersonService externalPersonService;

    @Autowired
    private InternalPersonService internalPersonService;

    @Autowired
    private ContactPersonService contactPersonService;

    @Autowired
    private AddressService addressService;

    @Autowired
    private CommunicationService communicationService;

    @Transactional
    public void anonymize( UUID contactUuid ) {
        TblContact tblContact = contactService.findTblContact(contactUuid);

        switch( tblContact.getContactType() ) {
            case Constants.CONTACT_TYPE_COMPANY:
                anonymizeCompany( tblContact.getUuid() );
                break;
            case Constants.CONTACT_TYPE_INTERNAL_PERSON:
                anonymizeInternalPerson( tblContact.getUuid() );
                break;
            case Constants.CONTACT_TYPE_EXTERNAL_PERSON:
                anonymizeExternalPerson( tblContact.getUuid() );
                break;
            case Constants.CONTACT_TYPE_CONTACT_PERSON:
                anonymizeContactPerson( tblContact.getUuid() );
                break;
            default:
                log.warn("Invalid Contact Type: "+tblContact.getContactType()+"for contact: "+contactUuid);
                throw new InternalServerErrorException("Invalid contact type found");
        }
    }

    private void anonymizeCompany( UUID contactUuid ) {
        CompanyDto dto = companyService.findCompany(contactUuid);
        dto.setCompanyName(Constants.ANONYMOUS_TAG);
        dto.setCompanyType(null);
        dto.setContactNote(getContactNote());
        dto.setContactAnonymized(true);
        companyService.updateCompany(dto);

        // kill 'em all
        companyService.findContactPersonsToCompany(contactUuid, false).stream()
                .forEach(contactPersonDto -> anonymizeContactPerson(contactPersonDto.getContactUuid()));

        processDependencies(contactUuid);
    }

    private void anonymizeInternalPerson( UUID contactUuid ) {
        InternalPersonDto internalPersonDto = internalPersonService.findInternalPerson(contactUuid);
        internalPersonDto.setLastName(Constants.ANONYMOUS_TAG);
        internalPersonDto.setFirstName(null);
        internalPersonDto.setTitle(null);
        internalPersonDto.setDepartment(null);
        internalPersonDto.setUid(null);
        internalPersonDto.setUserRef(null);
        internalPersonDto.setSalutationUuid(null);
        internalPersonDto.setContactNote(getContactNote());
        internalPersonDto.setContactAnonymized(true);

        internalPersonService.updateInternalPerson(internalPersonDto);

        processDependencies(contactUuid);
    }

    private void anonymizeExternalPerson( UUID contactUuid ) {
        ExternalPersonDto externalPersonDto = externalPersonService.findExternalPerson(contactUuid);
        externalPersonDto.setLastName(Constants.ANONYMOUS_TAG);
        externalPersonDto.setFirstName(null);
        externalPersonDto.setTitle(null);
        // leave personType untouched
        externalPersonDto.setSalutationUuid(null);
        externalPersonDto.setContactNote(getContactNote());
        externalPersonDto.setContactAnonymized(true);
        externalPersonService.updateExternalPerson(externalPersonDto);

        processDependencies(contactUuid);
    }

    private void anonymizeContactPerson( UUID contactUuid ) {
        ContactPersonDto contactPersonDto = contactPersonService.findContactPerson(contactUuid);
        contactPersonDto.setLastName(Constants.ANONYMOUS_TAG);
        contactPersonDto.setFirstName(null);
        contactPersonDto.setTitle(null);
        // leave personType untouched
        contactPersonDto.setSalutationUuid(null);
        contactPersonDto.setContactNote(getContactNote());
        contactPersonDto.setContactAnonymized(true);
        contactPersonService.updateContactPerson(contactPersonDto);

        processDependencies(contactUuid);
    }

    private void processDependencies(UUID contactUuid) {
        addressService.getAddressesByContactUuid(contactUuid).stream()
                .forEach(this::anonymizeAddress);

        communicationService.getCommunicationsByContactUuid(contactUuid).stream()
                .forEach(this::anonymizeCommunication);
    }

    private void anonymizeAddress( AddressDto addressDto ) {
        addressDto.setCommunity(Constants.ANONYMOUS_TAG);
        addressDto.setStreet(Constants.ANONYMOUS_TAG);
        addressDto.setCommunitySuffix(null);
        addressDto.setHousenumber(null);
        addressDto.setLatitude(null);
        addressDto.setLongitude(null);
        addressDto.setNote(null);
        addressDto.setPostcode(null);
        addressDto.setUrlMap(null);
        addressDto.setNote(Constants.ANONYMOUS_TAG);

        addressService.updateAddress(addressDto.getContactUuid(), addressDto);
    }

    private void anonymizeCommunication(CommunicationDto communicationDto ) {
        communicationDto.setCommunicationData(Constants.ANONYMOUS_TAG);
        communicationDto.setCommunicationTypeDescription(null);
        communicationDto.setNote(Constants.ANONYMOUS_TAG);

        communicationService.updateCommunication(communicationDto.getContactUuid(), communicationDto);
    }

    private static String getContactNote() {
        return new StringBuilder()
                .append("*** [")
                .append(new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(Date.from(Instant.now())))
                .append("] ***")
                .toString();
    }

}
