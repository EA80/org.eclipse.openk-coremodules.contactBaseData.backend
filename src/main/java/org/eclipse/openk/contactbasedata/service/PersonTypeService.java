/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.contactbasedata.service;

import lombok.extern.log4j.Log4j2;
import org.eclipse.openk.contactbasedata.constants.Constants;
import org.eclipse.openk.contactbasedata.exceptions.BadRequestException;
import org.eclipse.openk.contactbasedata.exceptions.NotFoundException;
import org.eclipse.openk.contactbasedata.mapper.PersonTypeMapper;
import org.eclipse.openk.contactbasedata.model.RefPersonType;
import org.eclipse.openk.contactbasedata.repository.PersonTypeRepository;
import org.eclipse.openk.contactbasedata.viewmodel.PersonTypeDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Log4j2
@Service
public class PersonTypeService {
    @Autowired
    private PersonTypeRepository personTypeRepository;

    @Autowired
    PersonTypeMapper personTypeMapper;

    public List<PersonTypeDto> findAllPersonTypes() {
        return personTypeRepository.findAll().stream()
                    .map( personTypeMapper::toPersonTypeDto )
                    .collect(Collectors.toList());
    }


    public PersonTypeDto getPersonTypeByUuid(UUID personTypeUuid) {
        RefPersonType refPersonType = personTypeRepository
                .findByUuid(personTypeUuid)
                .orElseThrow(() -> new NotFoundException(Constants.PERSON_TYPE_UUID_NOT_EXISTING));
        return personTypeMapper.toPersonTypeDto(refPersonType);
    }

    @Transactional
    public PersonTypeDto insertPersonType(PersonTypeDto personTypeDto) {
        RefPersonType personTypeToSave = personTypeMapper.toRefPersonType(personTypeDto);
        personTypeToSave.setUuid(UUID.randomUUID());

        RefPersonType savedPersonType = personTypeRepository.save(personTypeToSave);
        return personTypeMapper.toPersonTypeDto(savedPersonType);
    }

    @Transactional
    public PersonTypeDto updatePersonType(PersonTypeDto personTypeDto){
        RefPersonType personTypeUpdated;
        RefPersonType personTypeToSave = personTypeMapper.toRefPersonType(personTypeDto);
        RefPersonType existingPersonType = personTypeRepository
                .findByUuid(personTypeDto.getUuid())
                .orElseThrow(() -> new NotFoundException(Constants.PERSON_TYPE_UUID_NOT_EXISTING));
        personTypeToSave.setId(existingPersonType.getId());
        personTypeUpdated = personTypeRepository.save(personTypeToSave);

        return personTypeMapper.toPersonTypeDto(personTypeUpdated);
    }

    @Transactional
    public void removePersonType(UUID uuid) {
        RefPersonType existingPersonType = personTypeRepository.findByUuid(uuid)
                .orElseThrow( () -> new BadRequestException(Constants.PERSON_TYPE_UUID_NOT_EXISTING));

        personTypeRepository.delete(existingPersonType);
    }

}
