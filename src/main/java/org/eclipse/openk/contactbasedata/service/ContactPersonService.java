/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.contactbasedata.service;

import lombok.extern.log4j.Log4j2;
import org.eclipse.openk.contactbasedata.constants.Constants;
import org.eclipse.openk.contactbasedata.exceptions.NotFoundException;
import org.eclipse.openk.contactbasedata.mapper.ContactMapper;
import org.eclipse.openk.contactbasedata.mapper.ContactPersonMapper;
import org.eclipse.openk.contactbasedata.model.TblCommunication;
import org.eclipse.openk.contactbasedata.model.TblContact;
import org.eclipse.openk.contactbasedata.model.TblContactPerson;
import org.eclipse.openk.contactbasedata.repository.*;
import org.eclipse.openk.contactbasedata.viewmodel.ContactPersonDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

@Log4j2
@Service
public class ContactPersonService {

    @Autowired
    private ContactRepository contactRepository;

    @Autowired
    private ContactPersonRepository contactPersonRepository;

    @Autowired
    private CompanyRepository companyRepository;

    @Autowired
    private SalutationRepository salutationRepository;

    @Autowired
    private PersonTypeRepository personTypeRepository;

    @Autowired
    private ContactPersonMapper contactPersonMapper;

    @Autowired
    private ContactMapper contactMapper;


    public ContactPersonDto findContactPerson(UUID contactPersonUuid) {
        TblContactPerson tblContactPerson = contactPersonRepository
                                                            .findByTblContactUuid(contactPersonUuid)
                                                            .orElseThrow(() -> new NotFoundException(Constants.CONTACT_UUID_NOT_EXISTING));
       return setFromTblContactPerson(contactPersonMapper.toContactPersonDto(tblContactPerson), tblContactPerson);
    }

    @Transactional
    public ContactPersonDto insertContactPerson(ContactPersonDto contactPersonDto) {
        TblContact contactToSave = new TblContact();
        contactToSave.setUuid(UUID.randomUUID());
        contactToSave.setContactType(Constants.CONTACT_TYPE_CONTACT_PERSON);

        TblContactPerson contactPersonToSave = contactPersonMapper.toTblContactPerson(contactPersonDto);
        contactPersonToSave.setContact(contactToSave);
        contactRepository.save(contactPersonToSave.getContact());

        setFromContactPersonDto( contactPersonToSave, contactPersonDto );

        // Then save dependent Model-Object
        return setFromTblContactPerson(
                contactPersonMapper.toContactPersonDto(contactPersonRepository.save(contactPersonToSave)),
                contactPersonToSave);
    }

    @Transactional
    public ContactPersonDto updateContactPerson(ContactPersonDto contactPersonDto){
        TblContactPerson contactPersonUpdated;

        TblContactPerson existingContactPerson = contactPersonRepository
                .findByTblContactUuid(contactPersonDto.getContactUuid())
                .orElseThrow(() -> new NotFoundException(Constants.CONTACT_UUID_NOT_EXISTING));

        existingContactPerson.setLastName(contactPersonDto.getLastName());
        existingContactPerson.setFirstName(contactPersonDto.getFirstName());
        existingContactPerson.setTitle(contactPersonDto.getTitle());

        setFromContactPersonDto( existingContactPerson, contactPersonDto );
        contactPersonUpdated = contactPersonRepository.save(existingContactPerson);

        ContactPersonDto retDto = contactPersonMapper.toContactPersonDto(contactPersonUpdated);
        return setFromTblContactPerson(retDto, contactPersonUpdated);
    }

    @Transactional
    public void deleteContactPerson(UUID contactUuid) {
        TblContact tblContact = contactRepository.findByUuid(contactUuid)
                .orElseThrow(() -> new NotFoundException(Constants.CONTACT_UUID_NOT_EXISTING));
        TblContactPerson tblContactPerson = contactPersonRepository.findByTblContactUuid(contactUuid)
                .orElseThrow(() -> new NotFoundException("contact.person.uuid.not.existing"));

        contactPersonRepository.delete(tblContactPerson);
        contactRepository.delete(tblContact);
    }

    private ContactPersonDto setFromTblContactPerson( ContactPersonDto destDto, TblContactPerson srcTblContactPerson ) {
        TblCommunication tblComm = srcTblContactPerson.getContact()
                .getCommunications().stream()
                .filter(comm -> comm.getRefCommunicationType().isTypeEmail())
                .findFirst().orElse(null);

        destDto.setEmail(tblComm != null ? tblComm.getCommunicationData() : null );

        return destDto;
    }

    private void setFromContactPersonDto( TblContactPerson destTblContactPerson, ContactPersonDto sourceDto ) {

        if( sourceDto.getSalutationUuid() != null ) {
            destTblContactPerson.setSalutation( salutationRepository
                    .findByUuid(sourceDto.getSalutationUuid())
                    .orElseThrow(() -> new NotFoundException("salutation.uuid.not.existing")));
        }
        else {
            destTblContactPerson.setSalutation(null);
        }

        if( sourceDto.getPersonTypeUuid() != null ) {
            destTblContactPerson.setRefPersonType( personTypeRepository
                    .findByUuid(sourceDto.getPersonTypeUuid())
                    .orElseThrow(() -> new NotFoundException("person.type.uuid.not.existing")));
        }
        else {
            destTblContactPerson.setRefPersonType(null);
        }

        if( sourceDto.getCompanyContactUuid() != null ) {
            destTblContactPerson.setCompany( companyRepository
                    .findByTblContactUuid(sourceDto.getCompanyContactUuid())
                    .orElseThrow(() -> new NotFoundException("company.contact.uuid.not.existing")));
        }
        else {
            destTblContactPerson.setRefPersonType(null);
        }

        destTblContactPerson.getContact().setNote(sourceDto.getContactNote());
        destTblContactPerson.getContact().setAnonymized(sourceDto.getContactAnonymized());
    }
}
