/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.contactbasedata.mapper;

import org.eclipse.openk.contactbasedata.model.TblCommunication;
import org.eclipse.openk.contactbasedata.viewmodel.CommunicationDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface CommunicationMapper {

    @Mappings({
            @Mapping( source="tblContact.uuid", target="contactUuid"),
            @Mapping( source="refCommunicationType.uuid", target="communicationTypeUuid"),
            @Mapping( source="refCommunicationType.type", target="communicationTypeType"),
            @Mapping( source="refCommunicationType.description", target="communicationTypeDescription"),
    })
    CommunicationDto toCommunicationDto(TblCommunication tblCommunication);

    TblCommunication toTblCommunication(CommunicationDto communicationDto);
}