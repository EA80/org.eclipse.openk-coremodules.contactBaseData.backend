/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.contactbasedata.service;

import org.eclipse.openk.contactbasedata.config.TestConfiguration;
import org.eclipse.openk.contactbasedata.constants.Constants;
import org.eclipse.openk.contactbasedata.model.TblContact;
import org.eclipse.openk.contactbasedata.support.MockDataHelper;
import org.eclipse.openk.contactbasedata.viewmodel.AddressDto;
import org.eclipse.openk.contactbasedata.viewmodel.CommunicationDto;
import org.eclipse.openk.contactbasedata.viewmodel.CompanyDto;
import org.eclipse.openk.contactbasedata.viewmodel.ContactPersonDto;
import org.eclipse.openk.contactbasedata.viewmodel.ExternalPersonDto;
import org.eclipse.openk.contactbasedata.viewmodel.InternalPersonDto;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

@DataJpaTest
@ContextConfiguration(classes = {TestConfiguration.class})
@ActiveProfiles("test")
class ContactAnonymizerServiceTest {
    @Qualifier("myContactAnonymizerService")
    @Autowired
    private ContactAnonymizerService contactAnonymizerService;

    @MockBean
    private ContactService contactService;

    @MockBean
    private InternalPersonService internalPersonService;

    @MockBean
    private ExternalPersonService externalPersonService;

    @MockBean
    private CompanyService companyService;

    @MockBean
    private ContactPersonService contactPersonService;

    @MockBean
    private AddressService addressService;

    @MockBean
    private CommunicationService communicationService;

    @Test
    void shouldAnonymizeInternalPerson() {
        UUID theContactUuid = UUID.randomUUID();
        TblContact tblIntPers = MockDataHelper.mockTblContact();
        tblIntPers.setUuid(theContactUuid);
        tblIntPers.setContactType(Constants.CONTACT_TYPE_INTERNAL_PERSON);
        when( contactService.findTblContact(theContactUuid)).thenReturn(tblIntPers);

        InternalPersonDto ipDto = MockDataHelper.mockInternalPersonDto();
        ipDto.setContactUuid(theContactUuid);
        when( internalPersonService.findInternalPerson(theContactUuid))
                .thenReturn(ipDto);

        List<AddressDto> addressDtos = MockDataHelper.mockAddressDtoList();
        when( addressService.getAddressesByContactUuid(theContactUuid)).thenReturn(addressDtos);

        List<CommunicationDto> communicationDtos = MockDataHelper.mockCommunicationDtoList();
        when( communicationService.getCommunicationsByContactUuid(theContactUuid)).thenReturn(communicationDtos);

        assertNull( ipDto.getContactAnonymized());
        assertNotEquals( Constants.ANONYMOUS_TAG, addressDtos.get(0).getCommunity());
        assertNotEquals( Constants.ANONYMOUS_TAG, communicationDtos.get(0).getCommunicationData());

        contactAnonymizerService.anonymize(theContactUuid);

        assertTrue( ipDto.getContactAnonymized());
        assertEquals( Constants.ANONYMOUS_TAG , ipDto.getLastName());
        assertNull( ipDto.getFirstName());
        assertNull( ipDto.getSalutationUuid());
        assertEquals( Constants.ANONYMOUS_TAG, addressDtos.get(0).getCommunity());
        assertEquals( Constants.ANONYMOUS_TAG, communicationDtos.get(0).getCommunicationData());
    }

    @Test
    void shouldAnonymizeExternalPerson() {
        UUID theContactUuid = UUID.randomUUID();
        TblContact tblExtPers = MockDataHelper.mockTblContact();
        tblExtPers.setUuid(theContactUuid);
        tblExtPers.setContactType(Constants.CONTACT_TYPE_EXTERNAL_PERSON);
        when( contactService.findTblContact(theContactUuid)).thenReturn(tblExtPers);

        ExternalPersonDto epDto = MockDataHelper.mockExternalPersonDto();
        epDto.setContactUuid(theContactUuid);
        when( externalPersonService.findExternalPerson(theContactUuid))
                .thenReturn(epDto);

        List<AddressDto> addressDtos = MockDataHelper.mockAddressDtoList();
        when( addressService.getAddressesByContactUuid(theContactUuid)).thenReturn(addressDtos);

        List<CommunicationDto> communicationDtos = MockDataHelper.mockCommunicationDtoList();
        when( communicationService.getCommunicationsByContactUuid(theContactUuid)).thenReturn(communicationDtos);

        contactAnonymizerService.anonymize(theContactUuid);

        assertTrue( epDto.getContactAnonymized());
        assertEquals( Constants.ANONYMOUS_TAG, epDto.getLastName() );
        assertNull( epDto.getFirstName());
        assertNull( epDto.getSalutationUuid());
        assertEquals( Constants.ANONYMOUS_TAG, addressDtos.get(0).getCommunity());
        assertEquals( Constants.ANONYMOUS_TAG, communicationDtos.get(0).getCommunicationData());

    }


    @Test
    void shouldAnonymizeCompany() {
        UUID theContactUuid = UUID.randomUUID();
        TblContact tblCompany = MockDataHelper.mockTblContact();
        tblCompany.setUuid(theContactUuid);
        tblCompany.setContactType(Constants.CONTACT_TYPE_COMPANY);
        when( contactService.findTblContact(theContactUuid)).thenReturn(tblCompany);

        CompanyDto companyDto = MockDataHelper.mockCompanyDto();
        companyDto.setContactUuid(theContactUuid);
        when( companyService.findCompany(theContactUuid))
                .thenReturn(companyDto);

        List<ContactPersonDto> contactPersonDtos = MockDataHelper.mockContactPersonDtos();
        when( companyService.findContactPersonsToCompany(theContactUuid, false))
                .thenReturn(contactPersonDtos);

        UUID cp0UUID = contactPersonDtos.get(0).getContactUuid();
        UUID cp1UUID = contactPersonDtos.get(1).getContactUuid();
        when(contactPersonService.findContactPerson(cp0UUID)).thenReturn(contactPersonDtos.get(0));
        when(contactPersonService.findContactPerson(cp1UUID)).thenReturn(contactPersonDtos.get(1));

        // Addresses and communications for the company
        List<AddressDto> addressDtos = MockDataHelper.mockAddressDtoList();
        when( addressService.getAddressesByContactUuid(theContactUuid)).thenReturn(addressDtos);
        List<CommunicationDto> communicationDtos = MockDataHelper.mockCommunicationDtoList();
        when( communicationService.getCommunicationsByContactUuid(theContactUuid)).thenReturn(communicationDtos);

        // Addresses and communications for the first contact person
        List<AddressDto> addressDtosForCp = MockDataHelper.mockAddressDtoList();
        when( addressService.getAddressesByContactUuid(cp0UUID)).thenReturn(addressDtosForCp);
        when( addressService.getAddressesByContactUuid(cp1UUID)).thenReturn(new ArrayList<>());
        List<CommunicationDto> communicationDtosForCp = MockDataHelper.mockCommunicationDtoList();
        when( communicationService.getCommunicationsByContactUuid(cp0UUID)).thenReturn(new ArrayList<>());
        when( communicationService.getCommunicationsByContactUuid(cp1UUID)).thenReturn(communicationDtosForCp);

        contactAnonymizerService.anonymize(theContactUuid);

        assertTrue( companyDto.getContactAnonymized());
        assertEquals( Constants.ANONYMOUS_TAG, companyDto.getCompanyName() );
        assertNull( companyDto.getCompanyType());
        assertEquals( Constants.ANONYMOUS_TAG, addressDtos.get(0).getCommunity() );
        assertEquals( Constants.ANONYMOUS_TAG, communicationDtos.get(0).getCommunicationData() );

        assertTrue( contactPersonDtos.get(0).getContactAnonymized());
        assertTrue( contactPersonDtos.get(1).getContactAnonymized());
        assertEquals( Constants.ANONYMOUS_TAG, contactPersonDtos.get(0).getLastName() );
        assertEquals( Constants.ANONYMOUS_TAG, contactPersonDtos.get(1).getLastName() );
        assertNull( contactPersonDtos.get(0).getFirstName());
        assertNull( contactPersonDtos.get(1).getFirstName());
        assertEquals( Constants.ANONYMOUS_TAG, addressDtosForCp.get(0).getCommunity() );
        assertEquals( Constants.ANONYMOUS_TAG, communicationDtosForCp.get(0).getCommunicationData() );

    }
}
