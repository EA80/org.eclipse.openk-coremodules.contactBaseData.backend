/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */

package org.eclipse.openk.contactbasedata.service;

import org.assertj.core.util.Lists;
import org.eclipse.openk.contactbasedata.api.AuthNAuthApi;
import org.eclipse.openk.contactbasedata.config.TestConfiguration;
import org.eclipse.openk.contactbasedata.model.JwtToken;
import org.eclipse.openk.contactbasedata.model.LoginCredentials;
import org.eclipse.openk.contactbasedata.model.TblInternalPerson;
import org.eclipse.openk.contactbasedata.repository.CommunicationTypeRepository;
import org.eclipse.openk.contactbasedata.repository.InternalPersonRepository;
import org.eclipse.openk.contactbasedata.support.MockDataHelper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;

import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@DataJpaTest
@ContextConfiguration(classes = {TestConfiguration.class})
@ActiveProfiles("test")
class AuthNAuthServiceTest {
    private static final long MAIL_ID = 1L;
    private static final long TEL_ID = 2L;

    @Qualifier("myAuthNAuthService")
    @Autowired
    private AuthNAuthService authNAuthService ;

    @Autowired
    private AuthNAuthApi authNAuthApi;

    @MockBean
    private InternalPersonService internalPersonService;

    @MockBean
    private InternalPersonRepository internalPersonRepository;

    @MockBean
    private CommunicationTypeRepository communicationTypeRepository;

    @Test
    void shouldSynchronizeAuthnAuthRunThrou1() {
        List<TblInternalPerson> internalPeople = MockDataHelper.mockTblInternalPersonPage().getContent();
        internalPeople.get(1).setUserRef("karlK");
        when(internalPersonRepository.saveAll(any( List.class ))).thenReturn(Lists.emptyList());
        when( internalPersonRepository.findByUserRefNotNull() ).thenReturn(internalPeople);

        JwtToken jwtToken = new JwtToken();
        jwtToken.setAccessToken("ds2ds2d23ad3242");
        when( authNAuthApi.login(any(LoginCredentials.class))).thenReturn(jwtToken);
        when( authNAuthApi.logout(jwtToken.getAccessToken())).thenReturn(null);
        when( authNAuthApi.getKeycloakUsers(jwtToken.getAccessToken())).thenReturn(MockDataHelper.mockKeycloakUsers());

        authNAuthService.synchronizeAuthNAuth();

        verify( authNAuthApi, times(1)).logout(anyString());
        verify( internalPersonRepository, times(1 )).saveAll(any(List.class));
    }

}
