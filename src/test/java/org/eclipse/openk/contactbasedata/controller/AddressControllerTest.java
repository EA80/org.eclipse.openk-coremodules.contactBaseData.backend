/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.contactbasedata.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.eclipse.openk.contactbasedata.ContactBaseDataApplication;
import org.eclipse.openk.contactbasedata.service.AddressService;
import org.eclipse.openk.contactbasedata.support.MockDataHelper;
import org.eclipse.openk.contactbasedata.viewmodel.AddressDto;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;
import java.util.UUID;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(classes = ContactBaseDataApplication.class)
@AutoConfigureMockMvc
@ActiveProfiles("test")
class AddressControllerTest {

    @MockBean
    private AddressService addressService;

    @Autowired
    private MockMvc mockMvc;

    @Test
    void shouldReturnAddressesForAContact() throws Exception {
        List<AddressDto> listDtos = MockDataHelper.mockAddressDtoList();

        when(addressService.getAddressesByContactUuid(any(UUID.class))).thenReturn(listDtos);

        mockMvc.perform(get("/contacts/{uuid}/addresses", UUID.randomUUID()))
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON));
    }

    @Test
    void shouldReturnSingleAddress() throws Exception {
        AddressDto addressDto = MockDataHelper.mockAddressDto();

        when(addressService.getAddress(any(UUID.class), any(UUID.class))).thenReturn(addressDto);

        mockMvc.perform(get("/contacts/fc7f598b-0d51-46bb-9563-99851fe6a3ad/addresses/72e2ade0-376c-11ea-978f-2e728ce88125"))
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON));
    }


    @Test
    void shouldInsertAddress() throws Exception {
        AddressDto addressDto = MockDataHelper.mockAddressDto();
        addressDto.setUuid(UUID.fromString("1ad98d00-3774-11ea-850d-2e728ce88125"));
        addressDto.setCommunity("Mullewapp");
        addressDto.setCommunitySuffix("Nieder-");
        addressDto.setHousenumber("15");
        addressDto.setIsMainAddress(true);
        addressDto.setLatitude("52°N");
        addressDto.setLongitude("3°W");
        addressDto.setNote("Zufahrt über Hof");
        addressDto.setPostcode("12356");
        addressDto.setStreet("Weideweg");
        addressDto.setUrlMap("www.xyz");
        addressDto.setWgs84Zone("84");
        addressDto.setContactUuid(UUID.fromString("1ad98f8a-3774-11ea-850d-2e728ce88125"));
        addressDto.setAddressTypeType("Privat");
        addressDto.setAddressTypeDescription("???");
        addressDto.setAddressTypeUuid(UUID.fromString("5956e65e-3774-11ea-978f-2e728ce88125"));

        when(addressService.insertAddress(any(UUID.class), any(AddressDto.class)))
                .thenReturn(addressDto);

        mockMvc.perform(post("/contacts/{uuid}/addresses", addressDto.getContactUuid())
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(addressDto)))
                .andExpect(jsonPath("$.id", Matchers.is("1ad98d00-3774-11ea-850d-2e728ce88125")))
                .andExpect(jsonPath("$.community", Matchers.is("Mullewapp" )))
                .andExpect(jsonPath("$.communitySuffix", Matchers.is("Nieder-" )))
                .andExpect(jsonPath("$.housenumber", Matchers.is("15" )))
                .andExpect(jsonPath("$.isMainAddress", Matchers.is(true )))
                .andExpect(jsonPath("$.latitude", Matchers.is("52°N" )))
                .andExpect(jsonPath("$.longitude", Matchers.is("3°W" )))
                .andExpect(jsonPath("$.note", Matchers.is("Zufahrt über Hof" )))
                .andExpect(jsonPath("$.postcode", Matchers.is("12356" )))
                .andExpect(jsonPath("$.street", Matchers.is("Weideweg" )))
                .andExpect(jsonPath("$.urlMap", Matchers.is("www.xyz" )))
                .andExpect(jsonPath("$.wgs84Zone", Matchers.is("84" )))
                .andExpect(jsonPath("$.contactId", Matchers.is("1ad98f8a-3774-11ea-850d-2e728ce88125" )))
                .andExpect(jsonPath("$.addressTypeType", Matchers.is("Privat" )))
                .andExpect(jsonPath("$.addressTypeDescription", Matchers.is("???" )))
                .andExpect(jsonPath("$.addressTypeId", Matchers.is("5956e65e-3774-11ea-978f-2e728ce88125" )));

    }


    @Test
    void shouldUpdateAddress() throws Exception {

        AddressDto addressDto = MockDataHelper.mockAddressDto();
        when( addressService.updateAddress(any(UUID.class), any(AddressDto.class))).thenReturn(addressDto);

        mockMvc.perform(put("/contacts/{contactUuid}/addresses/{addressUuid}", addressDto.getContactUuid(), addressDto.getUuid())
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(addressDto)))
                .andExpect(status().is2xxSuccessful());
    }

    @Test
    void shouldNotUpdateAddressDueToInvalidArgs() throws Exception {

        AddressDto addressDto = MockDataHelper.mockAddressDto();
        addressDto.setAddressTypeUuid(null);
        when( addressService.updateAddress(any(UUID.class), any(AddressDto.class))).thenReturn(addressDto);

        mockMvc.perform(put("/contacts/{contactUuid}/addresses/{addressUuid}", addressDto.getContactUuid(), addressDto.getUuid())
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(addressDto)))
                .andExpect(status().is4xxClientError());
    }

    @Test
    void shouldNotUpdateAddress() throws Exception {

        AddressDto addressDto = MockDataHelper.mockAddressDto();
        when( addressService.updateAddress(any(UUID.class), any(AddressDto.class))).thenReturn(addressDto);

        mockMvc.perform(put("/contacts/{contactUuid}/addresses/{addressUuid}", addressDto.getContactUuid(), UUID.randomUUID().toString())
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(addressDto)))
                .andExpect(status().isBadRequest());
    }

    @Test
    void shouldDeleteAddress() throws Exception {
        mockMvc.perform(delete("/contacts/{contactUuid}/addresses/{addressUuid}", UUID.randomUUID(), UUID.randomUUID())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is2xxSuccessful());
    }


}